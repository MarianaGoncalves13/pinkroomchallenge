package com.example.myapplicationpinkroom.data.repository

import com.example.myapplicationpinkroom.data.api.ApiService
import com.example.myapplicationpinkroom.data.model.GitRepository
import com.example.myapplicationpinkroom.response.GitHubSearchResponse
import retrofit2.Response
import javax.inject.Inject

class Repository @Inject constructor(
    private val apiService: ApiService
) {
    suspend fun getRepositories(): Response<GitHubSearchResponse> {
        return apiService.getRepositories()
    }
}
