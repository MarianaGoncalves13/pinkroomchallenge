package com.example.myapplicationpinkroom.data

sealed class Result<out MODEL> {
    data class Success<out MODEL>(val data: MODEL) : Result<MODEL>()
    data class Error(val error: String) : Result<Nothing>()
    object Loading : Result<Nothing>()
}
