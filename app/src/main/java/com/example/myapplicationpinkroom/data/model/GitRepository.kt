package com.example.myapplicationpinkroom.data.model


import com.example.myapplicationpinkroom.data.api.ApiService
import com.example.myapplicationpinkroom.response.GitHubSearchResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOn
import retrofit2.http.Query
import javax.inject.Inject

@ExperimentalCoroutinesApi
data class GitRepository @Inject constructor(
    private val service: ApiService
) {

    fun getSearchResults() =
        object : NetWorkBoundRepository<GitHubSearchResponse, GitHubSearchResponse>() {
            override suspend fun fetchFromRemote() =
                service.getRepositories()

            override suspend fun apiMapper(body: GitHubSearchResponse?): GitHubSearchResponse = body!!
        }.asFlow().flowOn(Dispatchers.IO)
}