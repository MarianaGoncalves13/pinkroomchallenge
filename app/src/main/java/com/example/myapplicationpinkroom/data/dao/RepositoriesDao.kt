package com.example.myapplicationpinkroom.data.dao

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplicationpinkroom.R
import com.example.myapplicationpinkroom.response.GitHubSearchResponse
import com.example.myapplicationpinkroom.ui.adapter.SearchListAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import retrofit2.http.Query
import javax.sql.DataSource

@ExperimentalCoroutinesApi
interface RepositoriesDao {

//    @Query("SELECT * FROM Item")
//    fun getRepositories(): DataSource<GitHubSearchResponse.Item>
//}
//
//class GitRespositoriesViewModel(repositoriesDao: RepositoriesDao) : ViewModel() {
//    val repositoriesList: LiveData<PagedList<GitHubSearchResponse.Item>> =
//        RepositoriesDao.getRepositories().toLiveData(pageSize = 50)
//}
//
//class SearchFragment : AppCompatActivity() {
//
//    public override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        val viewModel: GitRespositoriesViewModel by viewModels()
//        val recyclerView = findViewById<RecyclerView>(R.id.recycleview_repositories)
//        val adapter = SearchListAdapter()
//        viewModel.repositoriesList.observe(this, PagedList(adapter::submitList))
//        recyclerView.adapter = adapter
//    }
//}
//
//
//class SeearchListAdapter() :
//    PagedListAdapter<GitHubSearchResponse, SearchListAdapter.ViewHolder>(DIFF_CALLBACK) {
//    fun onBindViewHolder(holder: SearchListAdapter.ViewHolder, position: Int) {
//        val repositories: GitHubSearchResponse? = getItem(position)
//
//        // Note that "concert" is a placeholder if it's null.
//        holder.bindTo(repositories)
//    }
//
//    companion object {
//        private val DIFF_CALLBACK = object :
//            DiffUtil.ItemCallback<GitHubSearchResponse>() {
//            // Concert details may have changed if reloaded from the database,
//            // but ID is fixed.
//            override fun areItemsTheSame(
//                oldResporitories: GitHubSearchResponse,
//                newRepositories: GitHubSearchResponse
//            ) = oldResporitories.items == newRepositories.items
//
//            override fun areContentsTheSame(
//                oldResporitories: GitHubSearchResponse,
//                newRepositories: GitHubSearchResponse
//            ) = oldResporitories == newRepositories
//        }
//    }
}
