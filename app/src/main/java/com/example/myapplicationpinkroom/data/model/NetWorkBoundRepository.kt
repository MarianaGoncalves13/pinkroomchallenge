package com.example.myapplicationpinkroom.data.model

import androidx.annotation.WorkerThread
import com.example.myapplicationpinkroom.data.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.Response

@ExperimentalCoroutinesApi
abstract class NetWorkBoundRepository<MODEL, API> {

    fun asFlow() = flow {
        emit(Result.Loading)
        val response = fetchFromRemote()
        val body = response.body()

        if (response.isSuccessful)
            emit(Result.Success(apiMapper(body)))
    }

    @WorkerThread
    protected abstract suspend fun fetchFromRemote(): Response<API>

    protected abstract suspend fun apiMapper(body: API?): MODEL
}
