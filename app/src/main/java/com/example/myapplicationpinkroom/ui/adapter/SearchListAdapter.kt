package com.example.myapplicationpinkroom.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplicationpinkroom.R
import com.example.myapplicationpinkroom.data.model.GitRepository
import com.example.myapplicationpinkroom.response.GitHubSearchResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class SearchListAdapter : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {

    private var itemList: MutableList<GitHubSearchResponse> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.item_repository_grid, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val item: GitHubSearchResponse = itemList[position]
        //holder.authorName.text =
        //holder.description.text = item.

    }

    override fun getItemCount(): Int = itemList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val repositoryName = itemView.findViewById<TextView>(R.id.textview_respository_name)
        val authorName =  itemView.findViewById<TextView>(R.id.textview_repository_owner)
        val description = itemView.findViewById<TextView>(R.id.textview_repository_description)
        val language = itemView.findViewById<TextView>(R.id.textview_repository_language)
        val rating = itemView.findViewById<RatingBar>(R.id.rating_repository)
    }
}