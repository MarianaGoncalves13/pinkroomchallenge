package com.example.myapplicationpinkroom.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.paging.PagingDataAdapter
import com.example.myapplicationpinkroom.R
import com.example.myapplicationpinkroom.databinding.FragmentSearchBinding
import com.example.myapplicationpinkroom.ui.adapter.SearchListAdapter
import com.example.myapplicationpinkroom.ui.viewmodels.GitRepositoriesViewModel
import javax.inject.Inject

class SearchFragment : Fragment(R.layout.fragment_search) {

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding

    private val viewModel: GitRepositoriesViewModel by viewModels()

    lateinit var searchListAdapter: SearchListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentSearchBinding.bind(view)
        showFragment(this)

        binding.apply {

        }
    }

    private fun showFragment(fragment: Fragment, addToBackStack: Boolean = true) {
        binding?.root?.clearFocus()
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.apply {
            setCustomAnimations(
                R.anim.slide_in_right,
                0
            )
            replace(R.id.main_content, fragment, null)
            if (addToBackStack) addToBackStack(null)
            commit()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }
    fun getGitHubSearch(){
        val params = hashMapOf("q" to "language: koltlin", "sort" to "stars", "order" to "ascend")
    }
}