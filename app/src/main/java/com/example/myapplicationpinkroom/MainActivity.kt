package com.example.myapplicationpinkroom

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.example.myapplicationpinkroom.ui.viewmodels.GitRepositoriesViewModel
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    private fun <T : Any> navigateToFragment(
        fragment: Class<T>,
        bundle: Bundle? = null,
        addToBackStack: Boolean = false,
        tag: String
    ){
        supportFragmentManager.commit {
            setCustomAnimations(
                R.anim.slide_in_right,
                0
            )
            replace(R.id.main_content, fragment as Class<Fragment>, bundle, tag)
            if(addToBackStack){
                clearBackStack()
                addToBackStack(null)
            }
        }
    }

    private fun clearBackStack(){
        val fragmentManager = supportFragmentManager
        for (i in 0 until  fragmentManager.backStackEntryCount)
            fragmentManager.popBackStack()
    }
}