package com.example.myapplicationpinkroom.di

import com.example.myapplicationpinkroom.data.api.ApiService
import com.example.myapplicationpinkroom.data.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
object DataRepositoryModule {

    @Provides
    fun provideDataRepository(apiService: ApiService): Repository {
        return Repository(apiService)
    }
}
