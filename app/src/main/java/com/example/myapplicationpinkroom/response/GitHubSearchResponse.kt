package com.example.myapplicationpinkroom.response

import com.google.gson.annotations.SerializedName

data class GitHubSearchResponse(
    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,
    @SerializedName("items")
    val items: List<Item>,
    @SerializedName("total_count")
    val totalCount: Int
) {
    data class Item(
        @SerializedName("description")
        val description: String,
        @SerializedName("id")
        val id: Int,
        val labelsUrl: String,
        @SerializedName("language")
        val language: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("owner")
        val owner: Owner,
        @SerializedName("score")
        val score: Int,
    )

    data class Owner(
        @SerializedName("avatar_url")
        val avatarUrl: String,
        @SerializedName("gravatar_id")
        val gravatarId: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("login")
        val login: String
    )
}
